import sys

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import QRegExp

from Core.PowMod import powMod


class PowModWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        loadUi('GUI/modPow.ui', self)
        self.aEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.aEdit))
        self.bEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.bEdit))
        self.nEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.nEdit))

    @pyqtSlot()
    def on_modBtn_clicked(self):
        if self.aEdit.text() != '' and self.bEdit.text() != '' and self.nEdit.text() != '':
            a = int(self.aEdit.text())
            b = int(self.bEdit.text())
            n = int(self.nEdit.text())
            self.result.setText(str(powMod(a, b, n)))
