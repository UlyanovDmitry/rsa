from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import QRegExp

from Core.PrimeTest import primeTest


class primeTestWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        loadUi('GUI/primeTest.ui', self)
        self.numberEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.numberEdit))

    @pyqtSlot()
    def on_checkBtn_clicked(self):
        if self.numberEdit.text() != '':
            p = int(self.numberEdit.text())
            if primeTest(p) is True:
                self.answerLabel.setText('Простое')
            else:
                self.answerLabel.setText('Составное')
