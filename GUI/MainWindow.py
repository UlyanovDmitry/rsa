import datetime

from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import QRegExp
from Core.RSA import generateRsaParams, codeRSA, decodeRSA
from Core.PrimeTest import primeTest
from math import gcd
from GUI.ExtendedGCDWindow import extendedGCDWindow
from GUI.ModPowWindow import PowModWindow
from GUI.PrimeTestWindow import primeTestWindow

class MainWindow(QMainWindow):
    def __init__(self, *args):
        super(MainWindow, self).__init__(*args)
        loadUi('GUI/mainWindow.ui', self)
        self.correctP = False
        self.correctQ = False
        self.correctE = False
        self.correctD = False
        self.uncheckedData = True
        self.extendedGcdDemo.triggered.connect(self.extendedGcdDemoClicked)
        self.modPowDemo.triggered.connect(self.modPowDemoClicked)
        self.primeTestDemo.triggered.connect(self.primeTestDemoClicked)
        self.pEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.pEdit))
        self.pEdit.textChanged.connect(self.pEdit_text_changed)
        self.qEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.qEdit))
        self.qEdit.textChanged.connect(self.qEdit_text_changed)
        self.eEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.eEdit))
        self.eEdit.textChanged.connect(self.eEdit_text_changed)
        self.dEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.dEdit))
        self.dEdit.textChanged.connect(self.dEdit_text_changed)

    def extendedGcdDemoClicked(self):
        self.extendedGcdDemoWindow = extendedGCDWindow()
        self.extendedGcdDemoWindow.show()

    def modPowDemoClicked(self):
        self.powModWindow = PowModWindow()
        self.powModWindow.show()

    def primeTestDemoClicked(self):
        self.primeTestWindow = primeTestWindow()
        self.primeTestWindow.show()

    @pyqtSlot()
    def on_generateBtn_clicked(self):
        params = {'bits': self.keySizeBox.value()}
        if self.correctP is True:
            params['p'] = int(self.pEdit.text())
        else:
            self.correctP = True
            self.pLabel.setStyleSheet('color : black')
        if self.correctQ is True:
            params['q'] = int(self.qEdit.text())
        else:
            self.correctQ = True
            self.qLabel.setStyleSheet('color : black')
        if self.correctE is True:
            params['e'] = int(self.eEdit.text())
        else:
            self.correctE = True
            self.eLabel.setStyleSheet('color : black')
        if self.correctD is True:
            params['d'] = int(self.dEdit.text())
        else:
            self.correctD = True
            self.dLabel.setStyleSheet('color : black')
        start = datetime.datetime.now()
        p, q, n, fi, e, d = generateRsaParams(**params)
        finish = datetime.datetime.now()
        self.uncheckedData = False
        self.generateInfoLabel.setText(str(finish-start))
        self.pEdit.setText(str(p))
        self.qEdit.setText(str(q))
        self.nEdit.setText(str(n))
        self.fiEdit.setText(str(fi))
        self.eEdit.setText(str(e))
        self.dEdit.setText(str(d))
        self.correctP = True
        self.correctQ = True
        self.correctE = True
        self.correctD = True
        self.uncheckedData = True

    @pyqtSlot()
    def on_codeBtn_clicked(self):
        self.on_codeClearBtn_clicked()
        if self.correctP and self.correctQ and self.correctE:
            message = self.messageEdit.text()
            e = int(self.eEdit.text())
            n = int(self.nEdit.text())

            start = datetime.datetime.now()
            codes = codeRSA(message, e, n)
            finish = datetime.datetime.now()
            self.codingInfoLabel.setText(str(finish - start))

            codedMessage = ""
            for code in codes:
                codedMessage += str(code) + ' '
            self.codeEdit.setText(codedMessage)

    @pyqtSlot()
    def on_decodeBtn_clicked(self):
        self.on_messageClearBtn_clicked()
        if self.correctP and self.correctQ and self.correctD:
            codedMessage = self.codeEdit.text().split(' ')
            codes = []
            for c in codedMessage:
                try:
                    codes.append(int(c))
                except ValueError:
                    None
            d = int(self.dEdit.text())
            n = int(self.nEdit.text())
            start = datetime.datetime.now()
            decodedMessage = decodeRSA(codes, d, n)
            finish = datetime.datetime.now()
            self.codingInfoLabel.setText(str(finish - start))
            self.messageEdit.setText(decodedMessage)

    @pyqtSlot()
    def on_messageClearBtn_clicked(self):
        self.messageEdit.setText("")

    @pyqtSlot()
    def on_codeClearBtn_clicked(self):
        self.codeEdit.setText("")

    def pEdit_text_changed(self):
        if self.uncheckedData is True:
            if self.pEdit.text() != '':
                p = int(self.pEdit.text())
                if primeTest(p) is True:
                    self.pLabel.setStyleSheet('color : green')
                    self.correctP = True
                else:
                    self.pLabel.setStyleSheet('color : red')
                    self.correctP = False
                self.n_fi_Edit_text_changed()
            else:
                self.nEdit.setText('')
                self.fiEdit.setText('')
                self.pLabel.setStyleSheet('color : black')
                self.correctP = False

    def qEdit_text_changed(self):
        if self.uncheckedData is True:
            if self.qEdit.text() != '':
                q = int(self.qEdit.text())
                if primeTest(q) is True:
                    self.qLabel.setStyleSheet('color : green')
                    self.correctQ = True
                else:
                    self.qLabel.setStyleSheet('color : red')
                    self.correctQ = False
                self.n_fi_Edit_text_changed()
            else:
                self.nEdit.setText('')
                self.fiEdit.setText('')
                self.qLabel.setStyleSheet('color : black')
                self.correctQ = False

    def n_fi_Edit_text_changed(self):
        if self.uncheckedData is True:
            if self.correctP and self.correctQ:
                p = int(self.pEdit.text())
                q = int(self.qEdit.text())
                self.nEdit.setText(str(p*q))
                self.fiEdit.setText(str((p - 1) * (q - 1)))
            else:
                self.nEdit.setText('')
                self.fiEdit.setText('')
            self.eEdit_text_changed()

    def eEdit_text_changed(self):
        if self.uncheckedData is True:
            if self.correctP and self.correctQ and self.eEdit.text() != '':
                e = int(self.eEdit.text())
                fi = int(self.fiEdit.text())
                if gcd(e, fi) == 1:
                    self.eLabel.setStyleSheet('color : green')
                    self.correctE = True
                else:
                    self.eLabel.setStyleSheet('color : red')
                    self.correctE = False
            else:
                self.eLabel.setStyleSheet('color : black')
                self.correctE = False
            self.dEdit_text_changed()

    def dEdit_text_changed(self):
        if self.uncheckedData is True:
            if self.correctE and self.dEdit.text() != '':
                e = int(self.eEdit.text())
                fi = int(self.fiEdit.text())
                d = int(self.dEdit.text())
                if e*d % fi == 1:
                    self.dLabel.setStyleSheet('color : green')
                    self.correctD = True
                else:
                    self.dLabel.setStyleSheet('color : red')
                    self.correctD = False
            else:
                self.dLabel.setStyleSheet('color : black')
                self.correctD = False
