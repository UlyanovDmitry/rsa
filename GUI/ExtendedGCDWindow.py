from PyQt5.QtCore import pyqtSlot
from PyQt5.QtWidgets import *
from PyQt5.uic import loadUi
from PyQt5.QtGui import QRegExpValidator
from PyQt5.QtCore import QRegExp

from Core.ExtendedEuclideanAlgorithm import gcdExtended


class extendedGCDWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        loadUi('GUI/extendedGCD.ui', self)
        self.aEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.aEdit))
        self.bEdit.setValidator(QRegExpValidator(QRegExp("\d+"), self.bEdit))

    @pyqtSlot()
    def on_calculateBtn_clicked(self):
        a = int(self.aEdit.text())
        b = int(self.bEdit.text())
        r, x, y = gcdExtended(a, b)
        self.resLabel.setText('''%s * %s + %s * %s = %s''' % (str(a), str(x), str(b), str(y), str(r)))
