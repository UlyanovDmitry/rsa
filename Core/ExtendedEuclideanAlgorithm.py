def gcdExtended(a, b):
    if b == 0:
        return a, 1, 0
    else:
        x = y = 0
        x1 = 0
        x2 = 1
        y1 = 1
        y2 = 0
        while b > 0:
            q = a//b
            r = a % b
            x = x2 - q * x1
            y = y2 - q * y1
            x2 = x1
            x1 = x
            y2 = y1
            y1 = y
            a = b
            b = r

        return a, x2, y2
