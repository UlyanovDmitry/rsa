import random
from .PrimeTest import primeTest

def generateSmallPrimeNumber(bits):
    while True:
        base = random.getrandbits(bits)
        p1 = 6*base + 1
        p2 = 6*base - 1
        if primeTest(p1) is True:
            return p1
        elif primeTest(p2) is True:
            return p2


def generateLargePrimeNumber(bits):
    while True:
        s = generateSmallPrimeNumber(bits//2 - 1)
        for i in range(0, s):
            r = random.randint(14, s)
            n = s*r + 1
            if primeTest(n) is True:
                return n
