from math import gcd
from Core.ExtendedEuclideanAlgorithm import gcdExtended

def generatePublicExponent(fi): #улучшить
    nonzerroBitsNumber = 2

    e = fi
    count = 0
    while e != 0:
        count += 1
        e >>= 1

    e = 1 << (count-1)

    while gcd(e+1, fi) != 1:
        e <<= 1

    return e + 1

def generatePrivateExponent(fi,e):
    d = gcdExtended(e, fi)[1]
    while d < 0:
        d += fi
    return d
