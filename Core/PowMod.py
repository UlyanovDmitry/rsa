def powMod(a, power, modulus):
    b = 1
    print(pow(a, power, modulus))
    while power != 0:
        if power & 1 == 0:
            power >>= 1
            a = (a * a) % modulus
        else:
            power -= 1
            b = (b*a) % modulus
    print(b)
    return b
