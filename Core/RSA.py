from .PrimeNumberGenerator import *
from .ExponentGenerator import generatePublicExponent, generatePrivateExponent
from multiprocessing import Process
from multiprocessing import Manager
from math import gcd

def multiprocessingGenerateFunction(variableName, returnDict, bits):
    returnDict[variableName] = generateLargePrimeNumber(bits)

def generateRsaParams(**kwargs):

    if kwargs.__contains__('bits'):
        if kwargs['bits'] >= 256 and not kwargs.__contains__('p') and not kwargs.__contains__('q'):
            manager = Manager()
            returnDict = manager.dict()
            pProcess = Process(target=multiprocessingGenerateFunction, args=('p', returnDict, kwargs['bits'],))
            pProcess.start()
            qProcess = Process(target=multiprocessingGenerateFunction, args=('q', returnDict, kwargs['bits'],))
            qProcess.start()

            if pProcess.is_alive():
                pProcess.join()
            if qProcess.is_alive():
                qProcess.join()
            p = returnDict['p']
            q = returnDict['q']
        else:
            if not kwargs.__contains__('p'):
                p = generateLargePrimeNumber(kwargs['bits'])
            if not kwargs.__contains__('q'):
                q = generateLargePrimeNumber(kwargs['bits'])
    if kwargs.__contains__('p'):
        p = kwargs['p']
    if kwargs.__contains__('q'):
        q = kwargs['q']

    n = p * q
    fi = (p - 1) * (q - 1)

    if kwargs.__contains__('e') and gcd(kwargs['e'], fi) == 1:
        e = kwargs['e']
        if kwargs.__contains__('d') and e*kwargs['d'] % fi == 1:
            d = kwargs['d']
        else:
            d = generatePrivateExponent(fi, e)
    else:
        e = generatePublicExponent(fi)
        d = generatePrivateExponent(fi, e)

    return p, q, n, fi, e, d


def codeRSA(message, e, n):
    codes = []
    code = 0
    for char in message:
        if ((code * 1000000) + ord(char)) < n: #исправить
            code = (code * 1000000) + ord(char)
        else:
            codes.append(pow(code, e, n))
            code = ord(char)
    if code != 0:
        codes.append(pow(code, e, n))
    return codes


def decodeRSA(codes, d, n):
    message = ""
    for code in codes:
        chars = pow(code, d, n)
        buffer = ""
        while chars > 0:
            buffer += chr(chars % 1000000)
            chars //= 1000000
        message += buffer[::-1]
    return message
